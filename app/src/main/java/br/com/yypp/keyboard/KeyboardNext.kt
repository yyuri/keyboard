package br.com.yypp.keyboard

import android.content.Context
import android.content.Intent
import android.inputmethodservice.InputMethodService
import android.inputmethodservice.Keyboard
import android.inputmethodservice.KeyboardView
import android.media.AudioManager
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputConnection
import android.widget.Button

/**
 * Created by ygor on 04/06/18.
 */

class KeyboardNext : InputMethodService(), KeyboardView.OnKeyboardActionListener {


    private var kv: KeyboardView? = null
    private var keyboard: Keyboard? = null
    private val btGallery: Button? = null
    private val btCamera: Button? = null

    private var caps = false


    override fun onPress(primaryCode: Int) {
        Log.d("subtype_pt_BR", "onPress")
    }

    override fun onRelease(primaryCode: Int) {
        Log.d("subtype_pt_BR", "onRelease")
    }

    override fun onKey(primaryCode: Int, keyCodes: IntArray) {
        Log.d("subtype_pt_BR", "onKey")
        val ic = currentInputConnection
        playClick(primaryCode)
        when (primaryCode) {
            Keyboard.KEYCODE_DELETE -> ic.deleteSurroundingText(1, 0)
            Keyboard.KEYCODE_SHIFT -> {
                caps = !caps
                keyboard!!.isShifted = caps
                kv!!.invalidateAllKeys()
            }
            Keyboard.KEYCODE_DONE -> ic.sendKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER))
            else -> {
                var code = primaryCode.toChar()
                if (Character.isLetter(code) && caps) {
                    code = Character.toUpperCase(code)
                }
                ic.commitText(code.toString(), 1)
            }
        }

    }

    override fun onText(text: CharSequence) {
        Log.d("subtype_pt_BR", "onText")
    }

    override fun swipeLeft() {
        Log.d("subtype_pt_BR", "swipeLeft")
    }

    override fun swipeRight() {
        Log.d("subtype_pt_BR", "swipeRight")
    }

    override fun swipeDown() {
        Log.d("subtype_pt_BR", "swipeDown")
    }

    override fun swipeUp() {
        Log.d("subtype_pt_BR", "swipeUp")
    }

    override fun onCreateInputView(): View {

        println("onCreateInputView.........")
        var view: View? = null
        val inflater1 = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        view = inflater1.inflate(R.layout.keyboard, null)
        view!!.invalidate()




//        replaceFragment(view.getContext(), NextKeyboardContactFragment.newInstance("", ""));
        //        FrameLayout content = (Button)view.findViewById(R.id.gallery);
//                startActivity(new Intent(this, MainActivity.class));
//        val intent = Intent(this, MainActivity::class.java)
//        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//        startActivity(intent)
        kv = view.findViewById<KeyboardView>(R.id.keyboard) as KeyboardView
        keyboard = Keyboard(this, R.xml.qwerty);
        kv?.setKeyboard(keyboard);
        kv?.setOnKeyboardActionListener(this);
        //        btGallery = (Button)view.findViewById(R.id.gallery);
        //        btGallery.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        ////                openCameraOrGallery("gallery");
        //            }
        //        });
        //        btCamera = (Button)view.findViewById(R.id.camera);
        //        btCamera.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        ////                openCameraOrGallery("camera");
        //            }
        //        });
        return view

        //        kv = (KeyboardView)getLayoutInflater().inflate(R.layout.keyboard, null);
        //        keyboard = new Keyboard(this, R.xml.qwerty);
        //        kv.setKeyboard(keyboard);
        //        kv.setOnKeyboardActionListener(this);
        //        return kv;
    }

    private fun playClick(keyCode: Int) {
        val am = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        when (keyCode) {
            32 -> am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR)
            Keyboard.KEYCODE_DONE, 10 -> am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN)
            Keyboard.KEYCODE_DELETE -> am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE)
            else -> am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD)
        }
    }

    fun replaceFragment(context: Context, frag: Fragment) {

        (context as FragmentActivity).supportFragmentManager
                .beginTransaction()
                //.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.container, frag, "TAG")
                .commitAllowingStateLoss()
    }
}
